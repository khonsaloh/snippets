#!/usr/bin/python3

from requests import get
from bs4 import BeautifulSoup as soup
from re import sub
#from time import sleep

headers = {
	'User-Agent': 'Mozilla/5.0 (X11; Linux i686; rv:68.0) Gecko/20100101 Firefox/68.0'
}
url = 'https://url.com/'

termino = url + argv[1]
peticion = get(termino, headers=headers)

criba = soup(peticion.content, 'html.parser')

resultado = criba.find('div', {'id' : 'resultados'})

a = resultado.text 

# regex para eliminar lineas en blanco
text = "\n".join([ll.rstrip() for ll in a.splitlines() if ll.strip()])
#print(text)
sust = sub("sustitucion", '', text)
