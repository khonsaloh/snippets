const puppeteer = require('puppeteer');

// function no need to be called later to execute
(async () => {
  someVar = 'hello'
})();

async myFunction () {
  const browser = await puppeteer.launch({
    headless: false,
    defaultViewport: null,
    slowMo:5
  });
  const page = await browser.newPage();
//  await page.click('selector');
//  await page.waitFor(2000); 2 miliseconds
//  await page.type('selectror', 'words to type');
//  await page.waitForSelector('selector');
//  const navigationPromise = page.waitForNavigation();
  
//  await navigationPromise
  await page.goto('page')
//  await page.setViewport({ width: 1346, height: 679 })
  
  //console.log()
  //#id
  //.class
  //'tag'
  //[atribute]
  await browser.close()
}

myFunction();
